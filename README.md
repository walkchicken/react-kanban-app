# Shrek Kanban

![GitLab](https://gitlab.com/walkchicken/react-kanban-app)

A Shrek kanban board built with React and Redux.

## Features

- Create projects, boards, lists, cards, labels and tasks
- Add card members, track time, set a due date, add attachments, write comments
- Markdown support in a card description and comment
- Filter by members and labels
- Customize project background
- Real-time updates
- User notifications
- Internationalization

## Development

Clone the repository and install dependencies:

```
git clone https://gitlab.com/walkchicken/react-kanban-app

```

```
cd react-kanban-app/client
pnpm install
```

```
cd react-kanban-app/server
pnpm install
```

Start the development server:
Create `server/.env` based on `server/.env.sample` and edit `DATABASE_URL` if needed, then initialize the database:

```
pnpm run server:db:init

pnpm start
```

Start the development client:

```
pnpm start
```

Demo user: demo@demo.demo demo

## Tech stack

- React, Redux, Redux-Saga, Redux-ORM, Semantic UI React, react-beautiful-dnd
- Sails.js, Knex.js
- PostgreSQL
