export default {
  translation: {
    common: {
      emailOrUsername: "E-mail hoặc tên tài khoản",
      invalidEmailOrUsername: "E-mail hoặc tên tài khoản không hợp lệ",
      invalidPassword: "Mật khẩu không hợp lệ",
      logInToPlanka: "Đăng nhập Shrek",
      noInternetConnection: "Không có kết nối Internet",
      pageNotFound_title: "Không tìm thấy trang",
      password: "Mật khẩu",
      projectManagement: "Quản lí dự án",
      serverConnectionFailed: "Kết nối máy chủ không thành công",
      unknownError: "Lỗi không xác định, hãy thử lại sau",
    },

    action: {
      logIn: "Đăng nhập",
    },
  },
};
